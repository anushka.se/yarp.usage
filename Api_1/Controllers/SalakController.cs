﻿using Api_1.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Frozen;

namespace Api_1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SalakController : ControllerBase
    {
        [HttpPost]
        [Route("create")]
        public IActionResult Create(MiriRequest miriRequest)
        {
            return Ok("Salak API is running..."+ miriRequest.Type);
        }
    }
}
