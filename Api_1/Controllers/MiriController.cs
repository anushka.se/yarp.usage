﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api_1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MiriController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Miri, benim salak.....");
        }
    }
}
