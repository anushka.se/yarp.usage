﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api_0.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StarController : ControllerBase
    {
        [HttpGet]
        [Route("m")]
        public IActionResult Get()
        {
            return Ok("stars API is running...");
        }
    }
}
